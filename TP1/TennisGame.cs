﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class TennisGame
    {
        private int scorePlayer1 = 0;
        private int scorePlayer2 = 0;
        private string scoreGame = "";
        public TennisGame() { }

        public int GetScorePlayer1 { get { return scorePlayer1; } }
        public void SetScorePlayer1()
        {
            if (scorePlayer1 == 3 && scorePlayer2 == 4)
            {
                scorePlayer2--;
            }
            scorePlayer1++;
        }
        public int GetScorePlayer2 { get { return scorePlayer2; } }
        public void SetScorePlayer2()
        {
            if (scorePlayer1 == 4 && scorePlayer2 == 3)
            {
                scorePlayer1--;
            }
            scorePlayer2++;
        }

        public string GetScoreGame { get { return scoreGame; } }

        public void PlayRound()
        {
            if(scorePlayer1 >= 4 || scorePlayer2 >= 4)
            {
                scoreGame = CheckScoreWin(scorePlayer1, scorePlayer2);

            }
            else if(scorePlayer1 <= 3 && scorePlayer2 <= 3)
            {
                scoreGame = CheckStandardScore(scorePlayer1, scorePlayer2);
            }
            else
            {
                throw new ArgumentOutOfRangeException("Score is not valid");
            }
        }

        public string CheckStandardScore(int scorePlayer1, int scorePlayer2)
        {

            if (scorePlayer1 == 0 && scorePlayer2 == 0)
            {
                scoreGame = "0:0";
            }
            else if (scorePlayer1 == 1 && scorePlayer2 == 0)
            {
                scoreGame = "15:0";
            }
            else if (scorePlayer1 == 2 && scorePlayer2 == 0)
            {
                scoreGame = "30:0";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 0)
            {
                scoreGame = "40:0";
            }
            else if (scorePlayer1 == 0 && scorePlayer2 == 1)
            {
                scoreGame = "0:15";
            }
            else if (scorePlayer1 == 0 && scorePlayer2 == 2)
            {
                scoreGame = "0:30";
            }
            else if (scorePlayer1 == 0 && scorePlayer2 == 3)
            {
                scoreGame = "0:40";
            }
            else if (scorePlayer1 == 1 && scorePlayer2 == 1)
            {
                scoreGame = "15:15";
            }
            else if (scorePlayer1 == 2 && scorePlayer2 == 2)
            {
                scoreGame = "30:30";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 3)
            {
                scoreGame = "40:40";
            }
            else if(scorePlayer1 == 1 && scorePlayer2 == 2)
            {
                scoreGame = "15:30";
            }
            else if(scorePlayer1 == 2 && scorePlayer2 == 1)
            {
                scoreGame = "30:15";
            }
            else if (scorePlayer1 == 1 && scorePlayer2 == 3)
            {
                scoreGame = "15:40";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 1)
            {
                scoreGame = "40:15";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 2)
            {
                scoreGame = "40:30";
            }
            else if (scorePlayer1 == 2 && scorePlayer2 == 3)
            {
                scoreGame = "30:40";
            }

            return scoreGame;
        }

        public string CheckScoreWin(int scorePlayer1, int scorePlayer2)
        {
            if (scorePlayer1 == 4 && scorePlayer2 == 0)
            {
                scoreGame = "Win:0";
            }
            else if(scorePlayer1 == 0 && scorePlayer2 == 4)
            {
                scoreGame = "0:Win";
            }
            else if(scorePlayer1 == 4 && scorePlayer2 == 1)
            {
                scoreGame = "Win:15";
            }
            else if(scorePlayer1 == 1 && scorePlayer2 == 4)
            {
                scoreGame = "15:Win";
            }
            else if(scorePlayer1 == 4 && scorePlayer2 == 2)
            {
                scoreGame = "Win:30";
            }
            else if (scorePlayer1 == 2 && scorePlayer2 == 4)
            {
                scoreGame = "30:Win";
            }
            else if (scorePlayer1 == 5 && scorePlayer2 == 3)
            {
                scoreGame = "Win:40";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 5)
            {
                scoreGame = "40:Win";
            }
            else if (scorePlayer1 == 4 && scorePlayer2 == 3)
            {
                scoreGame = "Advantage:40";
            }
            else if (scorePlayer1 == 3 && scorePlayer2 == 4)
            {
                scoreGame = "40:Advantage";
            }

            return scoreGame;

        }
    }
}
