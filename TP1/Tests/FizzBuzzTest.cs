using Xunit.Abstractions;
using TP1;

namespace Tests
{
    public class FizzBuzzTest
    {
        FizzBuzz _fb = new FizzBuzz();

        private static ITestOutputHelper _ouput;
        public FizzBuzzTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Trait("Category", "Generate interval")]
        [Fact]
        public void TestGenerateIntForCorrectInterval()
        {
            int value = 20;
            _fb.Generate(value);
            Assert.True(value >= 15 && value <= 150);
        }

        [Trait("Category", "Sequence Replace String")]
        [Fact]
        public void TestSequenceReplaceStrFizzMultiple3()
        {
            int value = 9;
            string result = _fb.SequenceReplaceStr(value);
            _ouput.WriteLine(result);
            Assert.True(result == "Fizz" && value % 3 == 0);
        }

        [Trait("Category", "Sequence Replace String")]
        [Fact]
        public void TestSequenceReplaceStrBuzzMultiple5()
        {
            int value = 20;
            string result = _fb.SequenceReplaceStr(value);
            _ouput.WriteLine(result);
            Assert.True(result == "Buzz" && value % 5 == 0);
        }

        [Trait("Category", "Sequence Replace String")]
        [Fact]
        public void TestSequenceReplaceStrFizzBuzzMultiple3And5()
        {
            int value = 15;
            string result = _fb.SequenceReplaceStr(value);
            _ouput.WriteLine(result);
            Assert.True(result == "FizzBuzz" && (value % 3 == 0 && value % 5 == 0));
        }

        [Trait("Category", "Sequence Replace String")]
        [Fact]
        public void TestSequenceReplaceStrNumberNotMultiple()
        {
            int value = 17;
            string result = _fb.SequenceReplaceStr(value);
            _ouput.WriteLine(result);
            Assert.True(result == value.ToString() && !(value % 3 == 0 && value % 5 == 0));
        }
    }
}