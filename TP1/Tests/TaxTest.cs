﻿using Microsoft.VisualStudio.TestPlatform.Utilities;
using TP1;
using Xunit.Abstractions;

namespace Tests
{
    public class TaxTest
    {
        Tax _tax = new Tax();

        private static ITestOutputHelper _ouput;
        public TaxTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Trait("Category", "Scale tax")]
        [Theory]
        [InlineData(-10, 0)]
        [InlineData(0, 0)]
        [InlineData(10000, 0)]
        [InlineData(20000, 1015)]
        [InlineData(32000, 3194)]
        [InlineData(96000, 24311)]
        [InlineData(180000, 59191)]
        public void TestScaleTax(int incomeTaxable, int expectedTaxableIncome)
        {
            int result = _tax.CalculateScaleTax(incomeTaxable);
            _ouput.WriteLine("Income taxable : " + result.ToString());
            Assert.Equal(expectedTaxableIncome, result) ;
        }

        [Trait("Category", "Rate tax")]
        [Theory]
        [InlineData(-10, 0)]
        [InlineData(0, 0)]
        [InlineData(10000, 0)]
        [InlineData(20000, 5)]
        [InlineData(32000, 10)]
        [InlineData(96000, 25)]
        [InlineData(180000, 33)]
        public void TestRateTax(int incomeTaxable, int expectedRateTax)
        {
            int totalTax = _tax.CalculateScaleTax(incomeTaxable);
            int result = _tax.CalculateRateTax(totalTax, incomeTaxable);
            Assert.Equal(expectedRateTax, result);
        }

        [Trait("Category", "Round value tax")]
        [Fact]
        public void TestCalculateRoundValue()
        {
            double value = 10.5;
            int result = _tax.CalculateRoundValue(value);
            Assert.Equal(11, result);
        }

    }
}
