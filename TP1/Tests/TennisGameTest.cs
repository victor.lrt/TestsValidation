﻿using Microsoft.VisualStudio.TestPlatform.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1;
using Xunit.Abstractions;
using static System.Formats.Asn1.AsnWriter;

namespace Tests
{
    public class TennisGameTest
    {
        TennisGame _game = new TennisGame();

        private static ITestOutputHelper _ouput;
        public TennisGameTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Theory]
        [InlineData(0, 0, "0:0")]
        [InlineData(1, 0, "15:0")]
        [InlineData(2, 0, "30:0")]
        [InlineData(3, 0, "40:0")]
        [InlineData(0, 1, "0:15")]
        [InlineData(0, 2, "0:30")]
        [InlineData(0, 3, "0:40")]
        [InlineData(1, 1, "15:15")]
        [InlineData(2, 2, "30:30")]
        [InlineData(3, 3, "40:40")]
        [InlineData(1, 2, "15:30")]
        [InlineData(2, 1, "30:15")]
        [InlineData(1, 3, "15:40")]
        [InlineData(3, 1, "40:15")]
        [InlineData(3, 2, "40:30")]
        [InlineData(2, 3, "30:40")]
        public void TestStandardScore(int scorePlayer1, int scorePlayer2, string expectedScore)
        {
            string result = _game.CheckStandardScore(scorePlayer1, scorePlayer2);
            _ouput.WriteLine(result);
            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(4, 0, "Win:0")]
        [InlineData(0, 4, "0:Win")]
        [InlineData(4, 1, "Win:15")]
        [InlineData(1, 4, "15:Win")]
        [InlineData(4, 2, "Win:30")]
        [InlineData(2, 4, "30:Win")]
        [InlineData(5, 3, "Win:40")]
        [InlineData(3, 5, "40:Win")]
        [InlineData(4, 3, "Advantage:40")]
        [InlineData(3, 4, "40:Advantage")]
        public void TestAdvantageWin(int scorePlayer1, int scorePlayer2, string expectedScore)
        {
            string result = _game.CheckScoreWin(scorePlayer1, scorePlayer2);
            _ouput.WriteLine(result);
            Assert.Equal(expectedScore, result);
        }






    }
}
