﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class FizzBuzz
    {
        public FizzBuzz()
        {
        }
        public string Generate(int n)
        {
            string result = "";
            for (int i = 1; i < n; i++)
            {
                result += SequenceReplaceStr(i) + " ";
            }

            return result;
        }

        public string SequenceReplaceStr(int n)
        {
            string replaceStr = "";
            if (n % 3 == 0 && n % 5 == 0)
            {
                replaceStr = "FizzBuzz";
            }
            else if (n % 3 == 0)
            {
                replaceStr = "Fizz";
            }
            else if (n % 5 == 0)
            {
                replaceStr = "Buzz";
            }
            else
            {
                replaceStr = n.ToString();
            }
            return replaceStr;
        }

    }
}
