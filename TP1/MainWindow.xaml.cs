﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TP1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TennisGame tennisGame = new TennisGame();

        public MainWindow()
        {
            InitializeComponent();
            /*
            Tax tax = new Tax();
            int income = 32000;
            int resTaxAmount = tax.CalculateScaleTax(income);
            int resTaxRate = tax.CalculateRateTax(resTaxAmount, income);
            tax.DisplayInfoTaxAmount(resTaxAmount);
            tax.DisplayInfoTaxRate(resTaxRate);
            */


        }

        private void btnAddPointPlayer1_Click(object sender, RoutedEventArgs e)
        {
            tennisGame.SetScorePlayer1();
            tennisGame.PlayRound();
            lblScore.Content = tennisGame.GetScoreGame;
        }

        private void btnAddPointPlayer2_Click(object sender, RoutedEventArgs e)
        {
            tennisGame.SetScorePlayer2();
            tennisGame.PlayRound();
            lblScore.Content = tennisGame.GetScoreGame;

        }
    }
}
