﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class Tax
    {

        public Tax()
        {
        }
        public int CalculateScaleTax(int incomeTaxable) // 20 000
        {
            double taxAmount = 0;
            double[] taxThresholds = { 10777, 27478, 78570, 168994, double.MaxValue };
            double[] taxRates = { 0, 0.11, 0.3, 0.41, 0.45 };

            for (int i = 0; i < taxRates.Length - 1; i++)
            {
                double currentTaxThresholds = taxThresholds[i];
                double currentTaxRates = taxRates[i];

                if (incomeTaxable < currentTaxThresholds && incomeTaxable < taxThresholds[i + 1] && i == 0)
                {
                    return 0;
                }
                else if (incomeTaxable < currentTaxThresholds && incomeTaxable < taxThresholds[i + 1] && i != 0)
                {
                    taxAmount += (incomeTaxable - taxThresholds[i - 1]) * currentTaxRates;
                    break;
                }
                else if (incomeTaxable > currentTaxThresholds && incomeTaxable < taxThresholds[i + 1] && i != 0)
                {
                    taxAmount += (currentTaxThresholds - taxThresholds[i - 1]) * currentTaxRates;
                }
                else if (incomeTaxable > currentTaxThresholds && incomeTaxable > taxThresholds[i + 1] && i != 0)
                {
                    taxAmount += (currentTaxThresholds - taxThresholds[i - 1]) * currentTaxRates;
                }

                if (i == taxRates.Length - 2)
                {
                    taxAmount += (incomeTaxable - currentTaxThresholds) * taxRates[i + 1];
                }

            }

            taxAmount = CalculateRoundValue(taxAmount);

            return (int)taxAmount;
        }

        public void DisplayInfoTaxAmount(double totalTax )
        {
            Console.WriteLine("Income taxable : " + totalTax.ToString() + " €");
        }
        public void DisplayInfoTaxRate(double rateTax)
        {
            Console.WriteLine("Rate taxable : " + rateTax.ToString() + " %");
        }

        public int CalculateRoundValue(double value)
        {
            int result = (int)Math.Round(value, MidpointRounding.AwayFromZero);
            return result;
        }

        public int CalculateRateTax(double totalTax, int baseIncome)
        {
            int result = 0;
            if (baseIncome == 0)
            {
                return result;
            }
            else
            {
                double totalRate = (totalTax / baseIncome) * 100;
                result = CalculateRoundValue(totalRate);
            }

            return result;
        }



    }
}
