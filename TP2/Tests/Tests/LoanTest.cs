using Microsoft.VisualStudio.TestPlatform.Utilities;
using System.Runtime.CompilerServices;
using TP2;
using Xunit.Abstractions;

namespace Tests.Tests
{
    public class LoanTest
    {
        Loan _loan = new Loan(75000, 10, 2.5);

        private static ITestOutputHelper _ouput;

        public LoanTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Fact]
        public void TestCalculateAnnualDurationToMonthly()
        {
            int result = _loan.CalculateAnnualDurationToMonthly();
            _ouput.WriteLine("Result : " + result.ToString());
            Assert.Equal(120, result);
        }

        [Fact]
        public void TestCalculateMonthlyLoan()
        {
            int loanExpected = 707;
            int loanCalculated = _loan.CalculateMonthlyLoan();
            _ouput.WriteLine("Loan Calculated : " + loanCalculated.ToString());
            Assert.Equal(loanExpected, loanCalculated);
        }

        [Fact]
        public void TestCalculateTotalAmountInterest()
        {
            int totalAmountInterestAmountExpected = 18750;
            int totalAmountInterestAmountCalculated = _loan.CalculateTotalAmountInterest();
            _ouput.WriteLine("Total Amount Interest Rate Calculated : " + totalAmountInterestAmountCalculated.ToString());
            Assert.Equal(totalAmountInterestAmountExpected, totalAmountInterestAmountCalculated);

        }

    }
}