using Microsoft.VisualStudio.TestPlatform.Utilities;
using System.Runtime.CompilerServices;
using TP2;
using Xunit.Abstractions;

namespace Tests.Tests
{
    public class CriteriaInsuranceTest
    {

        private static ITestOutputHelper _ouput;

        public CriteriaInsuranceTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Theory]
        [MemberData(nameof(TestCriteriaInsurance))]

        public void TestCalculateRateCriteriaInsurance(CriteriaInsurance criteria, double expectedResult)
        {
            _ouput.WriteLine("Rate Criteria Insurance Calculated : " + expectedResult.ToString());
            double result = criteria.CalculateRateCriteriaInsurance();
            Assert.Equal(expectedResult, result);
        }

        public static IEnumerable<object[]> TestCriteriaInsurance()
        {
            CriteriaInsurance criteria;
            yield return new object[] {
                criteria = new CriteriaInsurance() { Smoker = true, HeartProblems = true, WorkITEngineer = true }, criteria.RateBase +  (double)(0.15 + 0.3 - 0.05),
            };
            yield return new object[] {
                criteria = new CriteriaInsurance() { Athletic = true, Smoker = true, HeartProblems = true, WorkITEngineer = true, WorkFighterPilot = true },  criteria.RateBase +  (double)( - 0.05 + 0.15 + 0.3 - 0.05 + 0.15)
            };
            yield return new object[] {
                criteria = new CriteriaInsurance() { Athletic = false, Smoker = false, HeartProblems = false, WorkITEngineer = false, WorkFighterPilot = false }, criteria.RateBase + 0
            };
        }


    }
}