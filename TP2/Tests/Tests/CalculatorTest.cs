using Microsoft.VisualStudio.TestPlatform.Utilities;
using System.Runtime.CompilerServices;
using TP2;
using Xunit.Abstractions;

namespace Tests.Tests
{
    public class CalculatorTest
    {
        private Calculator _calculator = new Calculator();
        CriteriaInsurance _criteria = new CriteriaInsurance();
        Loan _loan = new Loan(75000, 10, 2.5);

        private static ITestOutputHelper _ouput;
        public CalculatorTest(ITestOutputHelper testOutput)
        {
            _ouput = testOutput;
        }

        [Fact]
        public void TestCalculateMonthlyInsurance()
        {
            int insuranceExpected = 18;
            int insuranceCalculated = _calculator.CalculateMonthlyAmountInsurance(_loan, _criteria);
            Assert.Equal(insuranceExpected, insuranceCalculated);
        }

        [Fact]
        public void TestCalculateMonthlyLoanWithInsurance()
        {
            int result = _calculator.CalculateMonthlyLoanWithInsurance(_loan, _criteria);
            int expectedResult = 725;
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void TestCalculateTotalInsurance()
        {
            int result = _calculator.CalculateTotalInsurance(_loan, _criteria);
            int expectedResult = 2160;
            Assert.Equal(expectedResult, result);
        }
    }
}