﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    public class CriteriaInsurance
    {

        public double RateBase { get; set; } = 0.3;
        public bool Athletic { get; set; } = false;
        public bool Smoker { get; set; } = false;
        public bool HeartProblems { get; set; } = false;
        public bool WorkITEngineer { get; set; } = false;
        public bool WorkFighterPilot { get; set; } = false;

        public double CalculateRateCriteriaInsurance()
        {
            double rate = RateBase;
            double rateAthletic = 0.05;
            double rateSmoker = 0.15;
            double rateHeartProblems = 0.3;
            double rateWorkITEngineer = 0.05;
            double rateWorkFighterPilot = 0.15;

            if (Athletic)
            {
                rate -= rateAthletic;
            }
            if (Smoker)
            {
                rate += rateSmoker;
            }
            if (HeartProblems)
            {
                rate += rateHeartProblems;
            }
            if (WorkITEngineer)
            {
                rate -= rateWorkITEngineer;
            }
            if (WorkFighterPilot)
            {
                rate += rateWorkFighterPilot;
            }
            return Math.Round(rate, 2);

        }

    }

}
