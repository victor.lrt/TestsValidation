﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TP2
{
    public class Loan
    {
        public int LoanAmount { get; set; }
        public int AnnualDuration { get; set; }
        public double AnnualInterestRate { get; set; }
        public int NbMonthsByYear { get; } = 12;
        public Loan(int loanAmount, int annualDuration, double annualInterestRate)
        {
            if(loanAmount < 50000)
            {
                throw new ArgumentException("Loan amount must be greater than 50000.");
            }
            if(annualDuration < 7 || annualDuration > 25)
            {
                throw new ArgumentException("Loan duration must be between 7 and 25.");
            }
            this.LoanAmount = loanAmount;
            this.AnnualDuration = annualDuration;
            this.AnnualInterestRate = annualInterestRate;
        }
        public int CalculateAnnualDurationToMonthly()
        {
            int resultMonthly = AnnualDuration * NbMonthsByYear;
            return resultMonthly;
        }

        public int CalculateMonthlyLoan()
        {
            int durationMonthly = CalculateAnnualDurationToMonthly();
            return (int)(LoanAmount * (AnnualInterestRate / NbMonthsByYear / 100) / (1 - Math.Pow(1 + (AnnualInterestRate / NbMonthsByYear / 100), -durationMonthly)));
        }

        public int CalculateTotalAmountInterest()
        {
            double percentageAnnualInterestRate = AnnualInterestRate / 100;
            double interestRateAmount = (LoanAmount * percentageAnnualInterestRate) * AnnualDuration;
            return (int)interestRateAmount;
        }


    }
}
