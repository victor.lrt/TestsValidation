﻿using System;
using System.Windows;

namespace TP2
{
    public class Calculator
    {

        public Calculator() { }

        public int CalculateMonthlyAmountInsurance(Loan loan, CriteriaInsurance criteria)
        {
            double criteriaCalculated = criteria.CalculateRateCriteriaInsurance();
            return ((int)(loan.LoanAmount * criteriaCalculated) / 100) / loan.NbMonthsByYear;
        }

        public int CalculateMonthlyLoanWithInsurance(Loan loan, CriteriaInsurance criteria)
        {
            int loanCalculated = loan.CalculateMonthlyLoan();
            int insuranceCalculated = CalculateMonthlyAmountInsurance(loan, criteria);
            return loanCalculated + insuranceCalculated;
        }
        public int CalculateTotalInsurance(Loan loan, CriteriaInsurance criteria)
        {
            int insuranceCalculated = CalculateMonthlyAmountInsurance(loan, criteria);
            return insuranceCalculated * loan.CalculateAnnualDurationToMonthly();
        }

    }



}
