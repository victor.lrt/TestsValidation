﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TP2
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CriteriaInsurance criteria = new CriteriaInsurance();
        public MainWindow()
        {
            InitializeComponent();
            ckbAthletic.IsChecked = criteria.Athletic;
            ckbSmoker.IsChecked = criteria.Smoker;
            ckbHeartProblems.IsChecked = criteria.HeartProblems;
            ckbWorkITEngineer.IsChecked = criteria.WorkITEngineer;
            ckbWorkFighterPilot.IsChecked = criteria.WorkFighterPilot;
            lblBasicRatePercentage.Content = criteria.RateBase.ToString() + "%";
        }

        private void btnCalculateMonthlyLoanAmount_Click(object sender, RoutedEventArgs e)
        {
            Calculator calcul = new Calculator();
            Loan loan = new Loan(Convert.ToInt32(txtLoanAmount.Text), Convert.ToInt32(txtDuration.Text), Convert.ToDouble(txtAnnualRate.Text));
            
            double resultLoanAmount = loan.CalculateMonthlyLoan();
            double resultTotalAmountInterest = loan.CalculateTotalAmountInterest();
            if (cbkWithInsurance.IsChecked == true)
            {
                double resultLoanAmountWithInsurance = calcul.CalculateMonthlyLoanWithInsurance(loan, criteria);
                double resultMonthlyAmountInsurance = calcul.CalculateMonthlyAmountInsurance(loan, criteria);
                double resultTotalAmountInsurance = calcul.CalculateTotalInsurance(loan, criteria);

                lblMonthlyLoanAmountWithInsurance.Content = resultLoanAmountWithInsurance + " €";
                lblMonthlyAmountInsurance.Content = resultMonthlyAmountInsurance + " €";
                lblTotalAmountInsurance.Content = resultTotalAmountInsurance.ToString() + " €";

            }
            else
            {
                lblMonthlyLoanAmountWithInsurance.Content = "0 €";
                lblMonthlyAmountInsurance.Content = "0 €";
            }

            lblTotalAmountInterest.Content = resultTotalAmountInterest.ToString() + " €";
            lblMonthlyLoanAmount.Content = resultLoanAmount.ToString() + " €";
        }

        private void ckbAthletic_Checked(object sender, RoutedEventArgs e)
        {
            criteria.Athletic = true;
        }
        private void ckbAthletic_Unchecked(object sender, RoutedEventArgs e)
        {
            criteria.Athletic = false;
        }
        private void ckbSmoker_Checked(object sender, RoutedEventArgs e)
        {
            criteria.Smoker = true;
        }
        private void ckbSmoker_Unchecked(object sender, RoutedEventArgs e)
        {
            criteria.Smoker = false;
        }

        private void ckbHeartProblems_Checked(object sender, RoutedEventArgs e)
        {
            criteria.HeartProblems = true;
        }
        private void ckbHeartProblems_Unchecked(object sender, RoutedEventArgs e)
        {
            criteria.HeartProblems = false;
        }
        private void ckbWorkITEngineer_Checked(object sender, RoutedEventArgs e)
        {
            criteria.WorkITEngineer = true;
        }
        private void ckbWorkITEngineer_Unchecked(object sender, RoutedEventArgs e)
        {
            criteria.WorkITEngineer = false;
        }
        private void ckbWorkFighterPilot_Checked(object sender, RoutedEventArgs e)
        {
            criteria.WorkFighterPilot = true;
        }
        private void ckbWorkFighterPilot_Unchecked(object sender, RoutedEventArgs e)
        {
            criteria.WorkFighterPilot = false;
        }


    }
}
